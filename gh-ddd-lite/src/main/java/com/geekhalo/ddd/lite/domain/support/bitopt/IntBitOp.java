package com.geekhalo.ddd.lite.domain.support.bitopt;


/**
 * Created by taoli on 17/6/20.
 */
public interface IntBitOp {
    boolean match(int value);

    String toSqlFilter(String fieldName);

    default IntBitOp or(IntBitOp other){
        return new IntBitOrOp(this, other);
    }

    default IntBitOp and(IntBitOp other){
        return new IntBitAndOp(this, other);
    }

    default IntBitOp not(){
        return new IntBitNotOp(this);
    }
}
